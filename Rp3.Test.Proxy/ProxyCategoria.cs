﻿using Rp3.Test.Common;
using Rp3.Test.Common.Response;
using Rp3.Test.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Proxy
{
    public class ProxyCategoria
    {
        public static ResponseCategoria GetTbCategoria(List<tbCategory> lstCategoria) {

            ResponseCategoria response = new ResponseCategoria();

            foreach (tbCategory item in lstCategoria)
            {
                Categoria cg = new Categoria();
                cg.CategoryId = item.CategoryId;
                cg.Name = item.Name;
                cg.Active = item.Active;
               

                response.data.Add(cg);
            }

            return response;
        }
    }
}
