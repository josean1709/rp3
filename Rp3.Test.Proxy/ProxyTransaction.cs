﻿using Rp3.Test.Common;
using Rp3.Test.Common.Response;
using Rp3.Test.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Proxy
{
    public class ProxyTransaction
    {

        public static ResponseTransaction GetResponseTransaction(List<tbTransaction> lstTransaction) {
            ResponseTransaction response = new ResponseTransaction();

            foreach (tbTransaction item in lstTransaction)
            {
                Transacciones tr = new Transacciones();
                tr.ClienteId = item.ClienteId;
                tr.Amount = item.Amount;
                tr.Notes = item.Notes;
                tr.RegisterDate = item.RegisterDate;
                tr.ShortDescription = item.ShortDescription;
                tr.TransactionId = item.TransactionId;
                tr.TransactionTypedId = item.TransactionTypedId;
                tr.CategoryId = item.CategoryId;

                response.data.Add(tr);
            }

            return response;
        }

        public static int save(Transacciones request)
        {
            ResponseTransaction response = new ResponseTransaction();

                tbTransaction tr = new tbTransaction();
                tr.ClienteId = request.ClienteId;
                tr.Amount = request.Amount;
                tr.Notes = request.Notes;
                tr.RegisterDate = request.RegisterDate;
                tr.ShortDescription = request.ShortDescription;
                tr.TransactionId = request.TransactionId;
                tr.TransactionTypedId = request.TransactionTypedId;
                tr.CategoryId = request.CategoryId;
            return new MTransacciones().save(tr);

               
        }

        public static ResponseTransactionT GetResponseTransactionT(List<tbTransactionType> lstTransactionT)
        {
            ResponseTransactionT response = new ResponseTransactionT();

            foreach (tbTransactionType item in lstTransactionT)
            {
                TransactionType tr = new TransactionType();
                tr.Name = item.Name;
                tr.TransactionTypeId = item.TransactionTypeId;
               

                response.data.Add(tr);
            }

            return response;
        }
    }
}
