﻿using Rp3.Test.Common;
using Rp3.Test.Common.Response;
using Rp3.Test.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Proxy
{
    public class ProxyCliente
    {
        public static ResponseCliente GetTbCliente(List<tbCliente> lstCliente) {
            ResponseCliente response = new ResponseCliente();

            foreach (tbCliente item in lstCliente)
            {
                Cliente cl = new Cliente();
                cl.ClienteId = item.ClienteId;
                cl.AccountNo = item.AccountNo;
                cl.Name = item.Name;
                
                response.data.Add(cl);
            }

            return response;

        }

    }
}
