﻿$(document).ready(function () {
    console.log("ready!");

    var tTransaccion;
    var category;
    var fecha;
    var obj = new Object();
    var fechaI;
    var fechaF;
    obj.clienteId = localStorage.getItem('Cliente');
    
    obj.fechaI = '08/09/1991';
    obj.fechaf = '08/09/1991';

    //GenerarBalanceTabla(obj)
    $.ajax({
        url: 'http://localhost:57794/api/Transacciones/GetTbTransactions',
        type: 'POST',
        data: obj,
        success: function (response) {
            console.log(response);
            GenerarBalanceTabla(response)
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });


    $.ajax({
        url: 'http://localhost:57794/api/TransactionT/GetTransactionT',
        type: 'POST',
        //data: obj,
        success: function (response) {
            console.log(response);
            LlenarTipoTr(response)
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });

    $.ajax({
        url: 'http://localhost:57794/api/Categoria/GetCategoria',
        type: 'POST',
        //data: obj,
        success: function (response) {
            console.log(response);
            LlenarCategoria(response)
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });
    

    

    });

function LlenarCategoria(data) {
    var resultado = data.data;

    $.each(resultado, function (key, registro) {
        var id = "";
        var nombre = "";
        $("#ctSelect").append('<option value=' + registro.CategoryId + '>' + registro.Name + '</option>');

    });

}

function LlenarTipoTr(data) {
    var resultado = data.data;

    $.each(resultado, function (key, registro) {
        var id = "";
        var nombre = "";
        $("#trtSelect").append('<option value=' + registro.TransactionTypeId + '>' + registro.Name + '</option>');

    });

}





$('#trtSelect').on('change', function () {
    tTransaccion = $(this).val();
    //alert(valor);
});


$('#ctSelect').on('change', function () {
    category = $(this).val();
    //alert(valor);
});

$('#txtFecha').on('change', function () {
    fecha = $(this).val();
    //alert(valor);
});

$('#txtFechaI').on('change', function () {
    fechaI = $(this).val();
    //alert(valor);
});

$('#txtFechaF').on('change', function () {
    fechaF = $(this).val();
    //alert(valor);
});


$("#saveTR").click(function () {
      var obj = new Object();


    console.log("Entre en la funcion Guardar");


    obj.TransactionTypedId = tTransaccion;
    var cliente = localStorage.getItem("Cliente");
    obj.ClienteId = cliente;

    obj.CategoryId = category;

    obj.RegisterDate = fecha;


    obj.ShortDescription = $("#txtDescripcion").val();


    obj.Amount = $("#txtvalor").val();

    obj.Notes = $("#txtNotas").val();

    

    $.ajax({
        url: 'http://localhost:57794/api/Transacciones/save',
        type: 'POST',
        data: obj,
        success: function (response) {
            console.log(response);
            GenerarBalanceTabla(response)
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });

    $("#popup").addClass("hide");

});

$("#btnBuscar").click(function () {
    var obj = new Object();

    obj.clienteId = localStorage.getItem('Cliente');

    obj.fechaI = fechaI;
    obj.fechaf = fechaF;

    //GenerarBalanceTabla(obj)
    $.ajax({
        url: 'http://localhost:57794/api/Transacciones/GetTbTransactions',
        type: 'POST',
        data: obj,
        success: function (response) {
            console.log(response);
            GenerarBalanceTabla(response)
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });

});

$("#btnNuevo").click(function () {
    $("#popup").removeClass("hide");

});

function GenerarBalanceTabla(response) {

    var $resultado = response.data;
    
        var $filas = '';
    $filas = ' <tr>' +
        '< th ></th >' +

        '<th>ID</th>' +
        '<th>Fecha</th>' +
        '<th>Categoria</th>' +
        '<th>Tipo</th>' +
        '<th>Descripcion</th>' +
        '<th>Valor</th>' +
        '<th>Notas</th>' +
        '</tr >';

        $.each($resultado, function (index, value) {
            console.log("value " + JSON.stringify(value));
          
                
            var $TransactionId = '<td class=".edit" title="">' + value.TransactionId + '</td>';
            var $RegisterDate = '<td class=".edit" title="">' + value.RegisterDate + '</td>';
            var $CategoryId = '<td class=".edit" title="">' + value.CategoryId + '</td>';
            var $TransactionTypedId = '<td class=".edit" title="">' + value.TransactionTypedId + '</td>';
            var $ShortDescription = '<td class=".edit" title="">' + value.ShortDescription + '</td>';
            var $Amount = '<td class=".edit" title="">' + value.Amount + '</td>';
            var $Notes = '<td class=".edit" title="">' + value.Notes + '</td>';
               
                $filas += '<tr class="odd" role="row">';
                $filas += $TransactionId;
                $filas += $RegisterDate;
                $filas += $CategoryId;
                $filas += $TransactionTypedId;
                $filas += $ShortDescription;
                $filas += $Amount;
                $filas += $Notes;

                $filas += '</tr>';
            console.log(value);
                
    

        });
    $("#tblTransaccion").html($filas);

}

