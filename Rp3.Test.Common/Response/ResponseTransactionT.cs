﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Common.Response
{
    public class ResponseTransactionT
    {
        public List<TransactionType> data { get; set; }

        public ResponseTransactionT() {

            this.data = new List<TransactionType>();
        }
    }
}
