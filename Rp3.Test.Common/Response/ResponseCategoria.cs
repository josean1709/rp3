﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Common.Response
{
    public class ResponseCategoria
    {
        public List<Categoria> data { get; set; }

        public ResponseCategoria()
        {

            this.data = new List<Categoria>();
        }
    }
}
