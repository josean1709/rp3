﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Common.Response
{
    public class ResponseCliente
    {
        public List<Cliente> data { get; set; }

        public ResponseCliente() {

            this.data = new List<Cliente>();
        }
    }
}
