﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Common
{
    public class Transacciones
    {
        public int TransactionId { get; set; }

        public short? TransactionTypedId { get; set; }

        public int? ClienteId { get; set; }

        public int? CategoryId { get; set; }

        public DateTime? RegisterDate { get; set; }

        
        public string ShortDescription { get; set; }

        
        public decimal? Amount { get; set; }

        public string Notes { get; set; }
    }
}
