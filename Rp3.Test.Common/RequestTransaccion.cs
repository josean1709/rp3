﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Common
{
    public class RequestTransaccion
    {
        public int clienteId { get; set; }

        public DateTime fechaI { get; set; }

        public DateTime fechaF { get; set; }
    }
}
