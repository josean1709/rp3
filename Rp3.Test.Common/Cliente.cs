﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Common
{
    public class Cliente
    {
        public int ClienteId { get; set; }

        public string AccountNo { get; set; }

        public string Name { get; set; }
    }
}
