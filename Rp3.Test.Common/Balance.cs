﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Common
{
    public class Balance
    {
        public Decimal remuneracion { get; set; }

        public Decimal bonos { get; set; }

        public Decimal alimentacion { get; set; }

        public Decimal transporte { get; set; }

        public Decimal total { get; set; }
    }
}
