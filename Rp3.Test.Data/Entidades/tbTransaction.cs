namespace Rp3.Test.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tbTransaction")]
    public partial class tbTransaction
    {
        [Key]
        public int TransactionId { get; set; }

        public short? TransactionTypedId { get; set; }

        public int? ClienteId { get; set; }

        public int? CategoryId { get; set; }

        public DateTime? RegisterDate { get; set; }

        [StringLength(100)]
        public string ShortDescription { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Amount { get; set; }

        public string Notes { get; set; }
    }
}
