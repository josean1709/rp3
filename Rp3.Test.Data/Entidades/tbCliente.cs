namespace Rp3.Test.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tbCliente")]
    public partial class tbCliente
    {
        [Key]
        public int ClienteId { get; set; }

        public string AccountNo { get; set; }

        public string Name { get; set; }
    }
}
