namespace Rp3.Test.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBRp3 : DbContext
    {
        public DBRp3()
            : base("name=DBRp3")
        {
        }

        public virtual DbSet<tbCategory> tbCategories { get; set; }
        public virtual DbSet<tbCliente> tbClientes { get; set; }
        public virtual DbSet<tbTransaction> tbTransactions { get; set; }
        public virtual DbSet<tbTransactionType> tbTransactionTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tbCategory>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbCliente>()
                .Property(e => e.AccountNo)
                .IsUnicode(false);

            modelBuilder.Entity<tbCliente>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<tbTransaction>()
                .Property(e => e.ShortDescription)
                .IsUnicode(false);

            modelBuilder.Entity<tbTransaction>()
                .Property(e => e.Amount)
                .HasPrecision(18, 6);

            modelBuilder.Entity<tbTransaction>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<tbTransactionType>()
                .Property(e => e.Name)
                .IsUnicode(false);
        }

        private void FixEfProviderServicesProblem()
        {
            // The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            // for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            // Make sure the provider assembly is available to the running application. 
            // See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
