﻿using Rp3.Test.Common;
using Rp3.Test.Common.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Data
{
    public class MTransacciones
    {

        private DBRp3 context;

        public MTransacciones() {
            context = new DBRp3();
        }

        public List<tbTransaction> GetTbTransactions(RequestTransaccion request) {
            // return DataBase.SqlQuery<Transacciones>("EXEC dbo.spGetBalance @AccountId ={0}, @FechaI = {1}, @Fechaf = {2}", request.cuentaId, request.fechaI, request.fechaF);
            List<tbTransaction> lstTransactions = new List<tbTransaction>();
            lstTransactions = context.Database.SqlQuery<tbTransaction>("EXEC dbo.SP_GET_TRANSACTION @clienteId ={0}, @FechaI = {1}, @Fechaf = {2}", request.clienteId, request.fechaI, request.fechaF).ToList();
            
            return lstTransactions;
        }

        public int save(tbTransaction request)
        {
            int resp = 0;

            try
            {
                var transaccion = this.context.Set<tbTransaction>();
                transaccion.Add(request);
                context.SaveChanges();
                resp = 1;
            }
            catch (Exception ex)
            {

                resp = -1;
            }


            return resp;
        }


        public List<tbTransactionType> GetTbTransactionsType()
        {
            // return DataBase.SqlQuery<Transacciones>("EXEC dbo.spGetBalance @AccountId ={0}, @FechaI = {1}, @Fechaf = {2}", request.cuentaId, request.fechaI, request.fechaF);
            List<tbTransactionType> lstTransactionsT = new List<tbTransactionType>();
            lstTransactionsT = context.Database.SqlQuery<tbTransactionType>("EXEC dbo.SP_GET_TRANSACTIONTYPE").ToList();

            return lstTransactionsT;
        }
    }
}
