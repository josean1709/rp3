﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Data.Modelos
{
    public class MCliente
    {
        private DBRp3 context;

        public MCliente()
        {
            context = new DBRp3();
        }

        public List<tbCliente> GetTbCliente()
        {
            // return DataBase.SqlQuery<Transacciones>("EXEC dbo.spGetBalance @AccountId ={0}, @FechaI = {1}, @Fechaf = {2}", request.cuentaId, request.fechaI, request.fechaF);
            List<tbCliente> lstCliente = new List<tbCliente>();
            lstCliente = context.Database.SqlQuery<tbCliente>("EXEC dbo.SP_GET_CLIENTE").ToList();

            return lstCliente;
        }
    }
}
