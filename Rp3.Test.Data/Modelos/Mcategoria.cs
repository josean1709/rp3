﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rp3.Test.Data.Modelos
{
    public class Mcategoria
    {
        private DBRp3 context;

        public Mcategoria()
        {
            context = new DBRp3();
        }

        public List<tbCategory> GetCategoria()
        {
            // return DataBase.SqlQuery<Transacciones>("EXEC dbo.spGetBalance @AccountId ={0}, @FechaI = {1}, @Fechaf = {2}", request.cuentaId, request.fechaI, request.fechaF);
            List<tbCategory> lstCategoria = new List<tbCategory>();
            lstCategoria = context.Database.SqlQuery<tbCategory>("EXEC dbo.SP_GET_CATEGORY").ToList();

            return lstCategoria;
        }
    }
}
