﻿using Rp3.Test.Common;
using Rp3.Test.Common.Response;
using Rp3.Test.Data;
using Rp3.Test.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Rp3.Test.WebApi.Data.Controllers
{
    public class TransaccionesController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public ResponseTransaction GetTbTransactions(RequestTransaccion request) {
            
            return ProxyTransaction.GetResponseTransaction(new MTransacciones().GetTbTransactions(request));
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public ResponseTransaction save(Transacciones request)
        {
            ResponseTransaction rp = new ResponseTransaction();
            if (ProxyTransaction.save(request) > 0) {

                RequestTransaccion rt = new RequestTransaccion();
                rt.clienteId = request.ClienteId.Value;
                rt.fechaI = request.RegisterDate.Value;
                rt.fechaF = rt.fechaI;
                return ProxyTransaction.GetResponseTransaction(new MTransacciones().GetTbTransactions(rt));
            }

            return rp;
        }

        


    }
}
