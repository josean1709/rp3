﻿using Rp3.Test.Common.Response;
using Rp3.Test.Data;
using Rp3.Test.Data.Modelos;
using Rp3.Test.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Rp3.Test.WebApi.Data.Controllers
{
    public class CategoriaController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public ResponseCategoria GetCategoria()
        {
            
            return ProxyCategoria.GetTbCategoria(new Mcategoria().GetCategoria());
        }
    }
}
